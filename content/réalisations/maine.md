---
title: "Fabrication d'une trémie dans une voûte"
description: "Modification d'une voûte pour pouvoir l'ouvrir en son milieu"
date: 2021-06-09T11:02:29+02:00
draft: false
featured_image: /images/realisations/tremievoute/maine01.webp
show_reading_time: true
toc: true
summary: "Pour pouvoir accéder à un sous-sol via un escalier intérieur, il a fallu percer une voûte en pierre."
lieu: /images/rues/avenuemaine.svg
nomdecode: avenue du Maine
tags:
- voûte
- trémie
---

## Enjeu

Cet appartement situé au rez-de-chaussée possédait une très belle cave voutée. Malheureusement, l’accès se faisait par les parties communes de l’immeuble.

L'idée était donc de réaliser un logement « souplex » en créant un escalier intérieur. Mais pour cela, il faut percer à travers la voûte et en démonter une partie en tenant compte des efforts en présence.

### Le principe d’une voûte

Sur une voûte, les pierres se bloquent les unes sur les autres en créant des poussées horizontales.

Dans sa partie haute, au niveau de la « clé de voûte », convergent toutes les forces qui maintiennent en équilibre l’ensemble des pierres. Si on enlève cette pierre, tout s’effondre !

{{< figure src="/images/realisations/tremievoute/maine03.webp" title="L'ensemble des forces convergent en un point singulier, la clé de voûte" class="w-60-l center">}}

Et plus la voûte est plate, plus les poussées horizontales sont importantes.

{{< figure src="/images/realisations/tremievoute/maine04.webp">}}

## Étude

La disposition intérieure du logement imposait la réalisation d’un escalier avec 1/8ᵉ de tournant et une longue trémie à la forme particulière, en travers de la voûte.

{{< figure src="/images/realisations/tremievoute/maine07.webp" title="Implantation de la trémie">}}

<div class="flex flex-wrap">

{{< figure src="/images/realisations/tremievoute/maine08.webp" class="w-48-l center" title="Coupe transversale">}}

{{< figure src="/images/realisations/tremievoute/maine09.webp" title="Coupe longitudinale" class="w-48-l center" >}}

</div>

### Principe d’une trémie dans une voûte

Comme on l'a vu, sur une voûte, les forces convergent vers la partie haute en créant des poussées horizontales. Pour percer une voûte, il faut donc réaliser un cadre qui supportera les efforts en les déroutant sur les côtés.

{{< figure src="/images/realisations/tremievoute/maine05.webp" title="Le cadre pourra être réalisé en béton ou en acier">}}

Il y a également un deuxième élément à prendre en compte : plus la trémie est longue, plus les forces à reprendre seront importantes mais aussi, plus le cadre va fléchir. Il ne va pas rompre, car il aura été calculé pour, mais il fléchira forcément un peu, peu importe qu'il soit en béton ou en acier.

Cette flexion aboutira inévitablement à une fissuration des joints de pierre, accompagnée d’un affaissement plus ou moins important.

Pour éviter ces mouvements qui peuvent créer des désordres dans les locaux adjacents, nous avons choisi d’effectuer une [mise en charge](/comprendre/mise-en-charge), c'est-à-dire que nous allons créer, dans notre cadre métallique, des forces égales et contraires aux forces à reprendre.

{{< figure src="/images/realisations/tremievoute/maine06.webp" title="Coupe de principe de renfort de trémie">}}

## Travaux

Nous commençons par ouvrir le sol du rez-de-chaussée selon la forme de la trémie, mais en prenant soin de ne pas toucher au parement inférieur, qui supporte les efforts.

<div class="flex flex-wrap">

{{< figure src="/images/realisations/tremievoute/maine10.webp" class="w-48-l center" title="Emplacement de la future trémie">}}

{{< figure src="/images/realisations/tremievoute/maine11.webp" title="Ouverture du sol en conservant soigneusement les pierres de la voûte" class="w-48-l center" >}}

</div>

Nous scellons ensuite des agrafes dans chaque pierre découverte et conservée. Ces agrafes seront ensuite noyées dans le béton, ce qui rendra le tout solidaire.

Ensuite, il est temps d'installer notre cadre métallique. Pour réaliser la mise en charge, nous avons mis le cadre sous tension en installant en son milieu des tiges filetées. Celles-ci resserrent les deux bords avec une force de 4 tonnes, selon le calcul effectué : cela a pour effet de fléchir les bords du cadre.

{{< figure src="/images/realisations/tremievoute/maine12.webp" title="Deux tiges filetées impriment une tension de plusieurs tonnes au cadre">}}

Une fois le cadre positionné, nous coffrons et ferraillons pour couler le béton qui maintiendra tout ensemble : les pierres conservées et le cadre métallique.

{{< figure src="/images/realisations/tremievoute/maine13.webp">}}

Après le durcissement du béton, nous pouvons décoffrer et couper les tiges filetées pour libérer les forces, à la manière d'un arc. Ces forces viendront en butée sur les pierres à maintenir.

Voilà, les pierres de voûte au droit de la trémie ne sont plus comprimées, nous pouvons donc les retirer sans problème et ouvrir la voûte.

<div class="flex flex-wrap">

{{< figure src="/images/realisations/tremievoute/maine14.webp" class="w-48-l center">}}

{{< figure src="/images/realisations/tremievoute/maine15.webp" class="w-48-l center" >}}

</div>

La trémie est réalisée, il ne reste plus qu'à mettre en place l'escalier fabriqué sur mesure.

<div class="flex flex-wrap">

{{< figure src="/images/realisations/tremievoute/maine16.webp" class="w-48-l center">}}

{{< figure src="/images/realisations/tremievoute/maine17.webp" class="w-48-l center" >}}

</div>

L’exiguïté des lieux nécessitait un encombrement minimum, calculé au centimètre.

<div class="flex flex-wrap">

{{< figure src="/images/realisations/tremievoute/maine18.webp" class="w-48-l center">}}

{{< figure src="/images/realisations/tremievoute/maine19.webp" class="w-48-l center" >}}

</div>

À présent, on peut profiter de ce magnifique sous-sol !

{{< figure src="/images/realisations/tremievoute/maine01.webp">}}

## Budget

Montant de l'opération : **23 000 € TTC** (sans l'escalier ni les finitions).
