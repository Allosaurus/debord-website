---
title: "Renforcement d'un plancher à poutrelles métalliques et hourdis plein"
description: "Consolidation de poutrelles rouillées suite à des dégâts des eaux"
date: 2021-06-08T22:08:32+02:00
draft: false
featured_image: /images/realisations/reille/reille10.webp
show_reading_time: true
toc: true
summary: "Un plancher à hourdis pleins a subi des dégâts des eaux qui ont affaibli ses solives métalliques. Il fallait déterminer l'ampleur de cet affaiblissement avant de réaliser un renforcement adapté"
lieu: /images/rues/avenuereille.svg
nomdecode: avenue Reille
tags:
- plancher
- confortement
---

## Enjeu

Suite à des dégâts des eaux, les solives de ce plancher à structure métallique et hourdis pleins ont partiellement rouillé et sont devenues trop faibles. Afin de calculer au mieux les renforts à mettre en place, une étude poussée s'est avérée nécessaire.

{{< figure src="/images/realisations/reille/reille2.webp" title="Découverte de la pièce avant intervention" >}}

## Étude

Dans le cadre de notre diagnostic précis, nous avons réalisé des sondages à différents endroits, pour mesurer les épaisseurs d’acier restant encore fonctionnel.

{{< figure src="/images/realisations/reille/reille13.webp" title="Les sondages sont masqués par de petites trappes">}}

Pour être encore plus précis dans nos calculs, nous avons prélevé des échantillons de fer que nous avons fait analyser par un laboratoire spécialisé, de façon à connaître leur résistance réelle. En effet, selon les dates de construction et les différentes aciéries, les fabrications de l’époque (1900 environ) n’étaient pas encore totalement normalisées et les caractéristiques des fers étaient assez variables.

<div class="flex flex-wrap">

{{< figure src="/images/realisations/reille/reille3.webp" title="Prélèvement d'échantillons" class="w-48-l center">}}

{{< figure src="/images/realisations/reille/reille14.webp" class="w-48-l center" >}}

</div>

Ces études ont démontré que ces solives étaient devenues insuffisantes, mais uniquement dans leur milieu : en effet, les efforts dans une poutre sont plus importants en son centre, et c’est d’ailleurs là qu’elles se rompent en général.

{{< figure src="/images/realisations/reille/reilleforces.webp">}}


Ce diagnostic nous a permis de conclure qu'il n'était pas nécessaire de renforcer les solives sur toute leur longueur. Nous avons donc pu concevoir des renforts additionnels n’allant pas jusqu’aux murs, ce qui nous a permis d’éviter de détruire les corniches.

## Travaux

Pour ce chantier, nous avons travaillé par le dessous, sans aucune intervention dans l’appartement du dessus.

Les solives ont toutes été renforcées les unes après les autres, pour éviter de déstabiliser les plafonds. À chaque fois, la procédure est la même.

Nous commençons par étayer le hourdis pour dégager le fer existant et accéder à celui-ci. Nous effectuons un piquage ainsi qu'un traitement antirouille. Cette étape permet également la décompression des maçonneries.

>La rouille gonfle énormément lorsqu'elle se forme (jusqu’à 7 fois le volume du fer), ce qui crée des contraintes colossales et des poussées néfastes. Il est donc intéressant de dégager les fers sur toute leur hauteur afin de relâcher ces pressions.

<div class="flex flex-wrap">

{{< figure src="/images/realisations/reille/reille4.webp" title="Dégagement d'un fer" class="w-48-l center">}}

{{< figure src="/images/realisations/reille/reille6.webp" title="Installation d'un premier renfort" class="w-48-l center" >}}

</div>

Nous insérons ensuite notre nouveau fer de renfort, et nous le [mettons en charge](/comprendre/mise-en-charge).

<div class="flex flex-wrap">

{{< figure src="/images/realisations/reille/reille7.webp" title="Dispositif de mise en charge" class="w-48-l center">}}

{{< figure src="/images/realisations/reille/reille8.webp" title="Armatures reconstituées avant blocage" class="w-48-l center" >}}

</div>

Ensuite, il nous faut reconstituer les fers secondaires d'armature des hourdis avant de bloquer le tout, reconstituer le hourdis et passer à la solive suivante.

<div class="flex flex-wrap">

{{< figure src="/images/realisations/reille/reille9.webp" title="Détail d'une entretoise" class="w-48-l center">}}

{{< figure src="/images/realisations/reille/reille10.webp" title="Ouverture pour la troisième solive après blocage des précédentes" class="w-48-l center" >}}

</div>

Une fois toutes les solives renforcées et bloquées, il ne nous reste plus qu'à refermer le plafond. Pour cela, nous mettons en place un grillage Nergalto sur les saignées puis nous rebouchons au plâtre, en nous raccordant au parties conservées, notamment les corniches.

<div class="flex flex-wrap">

{{< figure src="/images/realisations/reille/reille11.webp" title="Le grillage est en place avant réfection des plâtres" class="w-48-l center">}}

{{< figure src="/images/realisations/reille/reille5.webp" title="Les corniches ont été conservées" class="w-48-l center" >}}

</div>

{{< figure src="/images/realisations/reille/reille15.webp" title="Plafond refermé" class="center w-60-l">}}

Pour terminer, le peintre exprime tout son art, et l'intervention passe totalement inaperçue.

{{< figure src="/images/realisations/reille/reille12.webp">}}

## Budget

Montant de l'opération : **18 600 € TTC**.
