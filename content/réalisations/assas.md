---
title: "Confortement d'un plancher bois par le dessous avec rosace"
description: "Renforcement d'un plancher dans l'épaisseur en conservant corniches et rosace."
date: 2020-05-11T01:17:37+02:00
draft: false
featured_image: /images/realisations/repriseplancher/repriseplancher07.webp
show_reading_time: true
toc: true
summary: "Ce plancher d’appartement avec structure bois ancienne s’affaissait gravement. Nous l'avons renforcé par le dessous, en restant dans l’épaisseur du plancher et en conservant corniches et rosace."
lieu: /images/rues/rueassas.svg
nomdecode: rue d'Assas
tags:
- plancher
- confortement
---

## Enjeu

{{< figure src="/images/realisations/repriseplancher/repriseplancher01.webp" class="fr-l w-50-l" >}}

Dans un immeuble rue d'Assas, un plancher d'appartement s'affaissait gravement, et de manière évolutive. Pour le renforcer, il nous fallait trouver une solution fiable tout en conservant son authenticité à ce plancher. Le plafond en plâtre de l'appartement comprenait notamment une rosace et des corniches qu'il était intéressant de conserver.

## Étude

{{< figure src="/images/realisations/repriseplancher/repriseplancher02.webp" class="fr-l w-50-l mt0" >}}

Pour comprendre ce qui se passe, nous commençons par ouvrir une partie du plafond, et constatons que la structure bois ancienne est complètement pourrie. Une reprise générale de ce plancher donc été nécessaire.


>Le recalcul des capacités portantes de ces solives indique un léger sous-dimensionnement initial, auquel s'ajoute de fortes dégradations dues aux insectes et champignons xylophages.

La solution que nous avons proposée consiste à renforcer chaque solive dans l’épaisseur du plancher, conservant ainsi le principe de portée actuel ainsi que la hauteur sous plafond.

{{< figure src="/images/realisations/repriseplancher/repriseplancher03.webp" title="Renforcement des solives par des fers" >}}

{{< figure src="/images/realisations/repriseplancher/repriseplancher18.webp" title="Plan de l'intervention" >}}

### Note de calcul

Les plus courageux et les plus pointus peuvent télécharger les [notes de calcul ici](/pdf/assasnotedecalcul.pdf).

## Travaux

### Pose des fers

Après la mise en place d'un étaiement de sécurité, le plafond a été ouvert, mais en conservant la plupart des corniches périphériques d’origine, ainsi que la rosace centrale qu'il fallait sauvegarder.

{{< figure src="/images/realisations/repriseplancher/repriseplancher07.webp" title="Un chevêtre d'évitement a été réalisé pour sauvegarder la rosace" >}}

{{< figure src="/images/realisations/repriseplancher/repriseplancher08.webp" title="Les fers sont soudés, solidarisés au solives par tirefonds puis bloqués au plâtre" class="fr-l w-50-l mt0" >}}

Nous avons réalisé un nettoyage et un traitement des bois, car nous avons fait le choix de conserver ces solives anciennes. Après quoi nous avons pu passer à l'installation des fers. Chaque solive bois a été doublée et renforcée par des fers [UPE](/comprendre/quel-fer-choisir-) façonnés sur place.

Cette méthode a l'avantage d'être peu invasive, puisqu'on conserve la nature d'origine du plancher et que tout se fait dans l'épaisseur existante. Chaque profilé métallique est [mis en charge](/comprendre/mise-en-charge) pour reprendre immédiatement les charges de l'étage supérieur et d'éviter la poursuite, même temporaire, des affaissements. Cette mise en charge a aussi permis un rehaussement du plancher et la diminution de l'affaissement existant.

>Les structures métalliques sont calculées comme un complément aux bois d'origine, de manière à répondre aux contraintes de surcharges actuelles. Nous évitons tout surdimensionnement qui pourrait créer des « points durs » dans ces planchers relativement souples.


### Refermeture de l'ensemble

Nous mettons en œuvre un treillis métallique type Nergalto sur toute la surface du plafond. Ce treillis sert de support pour notre enduit en plâtre traditionnel, tout en renforçant la rigidité du plancher. La continuité de cette armature confère un supplément de rigidité et diminue les risques de fissuration qui existaient sur les supports traditionnels de ce "bacula", composés de petites lattes de bois de chataîgner.

{{< figure src="/images/realisations/repriseplancher/repriseplancher11.webp" title="Pose d'un treillis métallique" >}}

Nous réalisons également une bande pleine en maçonnerie de plâtre au centre, qui fait office d'entretoise. Cela permet de rigidifier encore un peu plus la structure.

{{< figure src="/images/realisations/repriseplancher/repriseplancher12.webp" title="Au centre, une bande de plâtre sur toute l'épaisseur" >}}

Il ne reste plus qu'à enduire au plâtre traditionnel.

{{< figure src="/images/realisations/repriseplancher/repriseplancher13.webp" title="La rosace et les corniches conservées" >}}

>Les protections en cours de chantier ont permis de conserver intacts les murs, les parquets, la cheminée ainsi que les éléments de cuisine.

{{< figure src="/images/realisations/repriseplancher/repriseplancher15.webp" title="Après peinture, le plafond ne porte aucune trace de l'intervention" >}}

{{< figure src="/images/realisations/repriseplancher/repriseplancher16.webp" title="L'appartement au-dessus n'a pas été impacté" >}}

## Budget

Montant de l'opération : **30 800 € TTC** (avec le plâtre).
