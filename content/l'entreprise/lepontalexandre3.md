---
title: "Le pont Alexandre III"
date: 2020-04-21T14:49:17+02:00
draft: false
featured_image: "/images/oldpont.jpg"
summary: "Le pont Alexandre III n'est pas seulement une œuvre d'art, c'est aussi une prouesse technique. Notre entreprise a eu la chance de travailler sur ce projet dès 1896."
toc: true
show_reading_time: true
---

Inauguré à l'occasion de l'exposition universelle de 1900, le pont Alexandre III reste le pont le plus élégant de Paris, grâce son abondante décoration sculptée de grande qualité. Il franchit la Seine entre les Champs-Élysées et les Invalides et se situe donc dans un site privilégié qu'il contribue à parfaire. Il doit aussi sa renommée à la prouesse technique qu'il représente. En effet, il est constitué d'une arche d'une seule volée qui enjambe la Seine dans un élan vigoureux, mais surbaissée pour ne pas rompre la perspective tant des Champs-Élysées que des Invalides.

{{< figure src="/images/oldpont.jpg" title="Le pont Alexandre III" >}}

Construit en deux ans seulement par les ingénieurs Résal et d'Alby (avec une pose de la première pierre en 1896 par le tsar Nicolas II), l'ouvrage fut inauguré lors de l'Exposition universelle de 1900. Ce pont, aussi grandiose qu'élégant, est classé monument historique.

Nous sommes fiers d’avoir participé à sa réalisation, au niveau des maçonneries de culées, très particulières. Car le surbaissement considérable des arcs (1/17) a nécessité des fondations de culées de dimensions très importantes, construites au moyen de caissons à air comprimé 33,50 m de long sur 44 m de large.

> Large de 40 mètres et légèrement biais, le pont Alexandre III comporte une arche métallique de 107 mètres à trois articulations, quinze fermes en acier moulé et deux viaducs en maçonnerie sur les rives.
