---
title: "Conditions générales"
description: ""
date: 2020-05-28T15:39:43+02:00
draft: false
featured_image: /images/lentreprise/
show_reading_time: false
toc: true
summary: ""
---


## Études

Nos études sont réalisées en deux temps.

### Étude préalable

Cette étude permet d’établir un diagnostic ainsi qu’un projet de travaux avec devis chiffré, elle est en général réalisée gratuitement par l’entreprise.
Afin de permettre cette première étude, la plus juste et précise possible, tous les renseignements, plans, rapports et historique doivent nous être fournis.
S’agissant de travaux touchant les structures du bâtiment, la visite pour diagnostic doit être organisée pour permettre l'examen simultané de tous les locaux mitoyens aux éléments étudiés.

- Pour un plancher par exemple, le tour des appartements des deux étages concernés est indispensable.
- Pour un mur porteur, les locaux des deux coté du mur, ainsi que ceux de l’étage au-dessus et ceux de l’étage du dessous doivent être connus.

### Étude d’exécution

Les études d’exécution sont réalisées dès la commande effectuée et définissent les détails et modalités d’intervention. Le coût de cette étude est indiqué au devis et fait partie de notre prestation.

Cette étude comportant les notes de calculs, définissant les détails d'exécution et la méthodologie de l'intervention est une nécessité pour permettre une bonne exécution de nos ouvrages. Les éléments permettent d'établir les documents et consignes destinés à nos équipes. La rédaction pour diffusion extérieure n’est pas prévue à priori.

La diffusion en parallèle au maître d'ouvrage est possible à titre informatif, mais il faut savoir que si son feu vert est nécessaire, ce qu'il doit préciser, nous attendrons son retour avant de lancer l'opération, ce qui peut engendrer un alongement du délai d'exécution de plusieurs semaines.

### Garantie des études

Nos études et diagnostics sont réalisés par notre bureau d’étude interne, notre assurance couvre ces études dans la mesure où les travaux sont réalisés par l’entreprise.

Les résultats ne peuvent servir de support à un appel d’offre ouvert à d'autres entreprises que dans la mesure où la responsabilité est transférée au rédacteur de l’appel d’offre qui devient alors prescripteur et ne pourra se retourner contre nous.

## Devis

Dans notre secteur d’activité, l'établissement d'un devis nécessite en général un gros investissement préalable en diagnostic, études et déplacements.

Pour simplifier les démarches administratives et les temps d’intervention, les études préalables et devis sont réalisés gratuitement, sauf indication contraire.

Un rapport avec devis chiffré est remis.

En contrepartie de ce travail, il est demandé au client qui s’y engage, de nous informer des décisions qui seront finalement prises s'il ne nous commande pas les travaux : une autre entreprise a fait une proposition plus adaptée ? Laquelle ? Le projet est-il abandonné ?

Ces informations nous sont utiles afin d’améliorer nos offres et de nous maintenir compétitifs.

Nos devis sont proposés à prix fixe, les prix unitaires et les quantités éventuellement mentionnées le sont à titre informatif, l'étendu de nos prestations étant bornée à l'ouvrage défini.

Sauf indication contraire, nos devis ne nous engagent que pendant la période de 6 mois qui suit la date de leur établissement. Tout changement dans la situation règlementaire, fiscale, l'évolution du sinistre, l'accessibilité ou l’état des lieux visités donnerait lieu à actualisation de notre offre.

## Prix

Nos prix sont fermes et définitifs. Ils englobent en général tous les aléas de chantier et découvertes ultérieures.

Aucun supplément ne sera facturé sauf à la demande directe ou indirecte du maitre d’ouvrage.

En contrepartie, aucun rabais ou remise commerciale ne sont pratiqués.

Les seuls éléments pouvant donner lieu à suppléments seraient : problème d’accès, restriction d’horaire ou de jour, étendue de la prestation, contrainte de réalisation particulière demandée par le maître d'ouvrage ou son représentant, ou découlant de sa responsabilité.

## Mise en concurrence

La mise en concurrence de différentes entreprises autour d’un problème à solutionner est souvent nécessaire afin de trouver la solution la meilleure et la moins chère (la «mieux-disante »).

Il est interdit de diffuser les devis reçus avant que toutes les entreprises aient établi le leur.

Par contre, une fois tous les devis en main, la diffusion est possible et même souhaitée pour aider à la formation continue des bureaux d’étude qui ont travaillé sur le projet et qui souhaitent avoir un retour sur leur travail.

C’est parce que la majorité de nos devis est effectivement commandée que nous pouvons nous permettre leur gratuité.

Bien que nous travaillions toujours avec la perspective d’une mise en concurrence, celle-ci n'est pas systématique.

## Communication et utilisation des résultats de nos prestations

Les résultats de nos prestations sont consignés dans des comptes rendus ou rapports, note de calculs et plans qui sont établis au format informatique (PDF).

Aucune modification ou altération ne pourra être portée à ces documents après leur communication sans notre accord écrit.

La reproduction d'un document établi par DEBORD SAS n'est autorisée que sous sa forme intégrale, et pour une utilisation conforme à sa destination.

## Commande

Toute demande de prestations doit faire l'objet d'une commande en bonne et due forme établie par le donneur d'ordres, dénommé aussi client ou « maître d’ouvrage ».

Cette commande devra comporter :
    1. Le devis accepté, daté et signé par le client avec ses coordonnées complètes.
    2. Le versement effectif d’un acompte à la commande de 30%.
    3. L’attestation de TVA à taux réduit si les travaux y donnent droit
    4. Toutes les indications nécessaires à la réalisation.
    5. Les clés ou les moyens d’accès.

Les commandes par voies électroniques sont acceptées, le paiement de l’acompte validant pour le maître d'ouvrage la teneur de ces documents.

Toute commande est définitive, car engage immédiatement l’entreprise à des frais d’étude et de mise en place. En cas d’annulation avant exécution, le paiement intégral pourrait légalement être exigé, mais en pratique, pour un client de bonne foi, nous ne facturons que le coût de l’investissement réellement engagé.

Toute commande implique l'acceptation par le donneur d'ordre des présentes conditions générales. Aucune clause contraire même si elle figure sur les documents de commande ou les conditions générales du donneur d'ordres ne nous est opposable en l'absence d'accord écrit de notre part.


## Délai d’intervention

Sauf stipulation contraire, nos délais d’intervention normaux sont de 10 jours ouvrés.

Ce délai bien que respecté à 90% n’est pas garanti.

Ce délai démarre lorsque tous les éléments sont réunis : devis signé, acompte versé, attestation TVA signée le cas échéant, toutes les autorisations obtenues, tous les accès possibles sans contrainte, les clés doivent nous être remises préalablement ou être disponibles sur place sans RDV. (Voisin, gardien, commerçant etc…).

Il est précisé que le temps de réalisation des chantiers est parfois difficile à déterminer précisément, en fonction des nombreux aléas sur les bâtiments anciens. Il est donc délicat de planifier précisément l'ouverture du chantier dépendant de l'achèvement du précédent.

Une incertitude demeure toujours quant au jour et l’heure de démarrage annoncé.

La priorité est donnée aux appartements habités. Les travaux en caves par exemple, qui ne dérangent personne, et peuvent être réalisés par tout temps, sur des périodes plus longues, et sans avoir à déterminer une date précise au jour près.

Nombre de maîtres d'ouvrage le comprennent et nous permettent cette souplesse.

## Délai d’exécution – Pénalité de retard

Les délais d’exécution sont indiqués à titre informatif mais ne constituent pas un engagement contractuel. De fait, aucune pénalité de retard n’est prévue.

En cas de réelle nécessité, un délai contractuel avec pénalité de retard peut être demandé mais le prix sera calculé en tenant compte de cette contrainte et des complications administratives qui y sont liées.

## Désordres collatéraux

Certaines interventions peuvent entraîner des dommages tels que des fissurations, éclats, décollement de revêtement, etc, sur les sites d'intervention.

Les protections mises en place ne peuvent être efficaces à 100%, leur installation systématique peut aussi être contreproductive. Par exemple, les protections aux sols aux endroits de passage peuvent parfois entraîner des chutes de personnes ; les adhésifs de maintien des protections peuvent arracher une peinture en mauvais état, ou laisser des traces de propreté préjudiciables.

Ces désordres sont consécutifs à nos interventions sur les structures et ne sont pas considérés comme des accidents à prendre en charge par les assurances.

Les remises en état, indemnisations ou réparations correspondantes sont à la charge du donneur d'ordres qui s’y engage.

## Limite de prestation – nettoyage

Nos travaux s’insèrent généralement dans le cadre d’un projet global en ne constituant que le lot gros-œuvre – charpente qui doit être suivi d’autres corps d’état, de peinture notamment.

Notre intervention en tient compte et nous prévoyons un nettoyage de chantier courant, enlèvement de nos gravats et déchets, mais sans dépoussiérage complet ni reprises de peintures, éclats et petits défauts.

## Réception des travaux

La réception de nos travaux est prononcée dès l'achèvement de ceux-ci, au dernier jour du chantier.

Il s’agit d’une réception tacite.

Elle est officialisée par l’établissement de la facture.

Toute contestation quant à cette réception doit être faite sous 15 jours.

Une réception officielle contradictoire peut toutefois être organisée sur demande et pourra éventuellement faire l’objet d’une facturation.

Lors de réalisation faisant intervenir plusieurs corps d’état, une réception partielle portant sur nos seuls ouvrages sera considérée effective, sans attendre la réception globale de tous les travaux.

## Facture

La facture formalise la fin du chantier et marque le départ des délais de paiement et des différentes garanties.

Nos factures sont à régler sous 15 jours à la date d’émission, sauf stipulation différente.

En cas de retard de paiement, seront exigés: 40 € forfaitaires par relance, + 1,5 % d’intérêts par mois ou fraction de mois, ainsi que tous autres frais de poursuite ou préjudice.

Aucune facturation ne pourra être contestée passé 30 jours après son émission.

## Retenue de garantie

Aucune retenue de garantie n’est prévue sur nos travaux, sauf accord contraire.

En effet, cette précaution n'est pas adaptée à nos travaux de reprise de structure, la somme de 5% couramment pratiquée est insuffisante pour garantir la réparation du moindre défaut, et un an semble dérisoire au regard de la durée de vie espérée de nos renforts.

Par contre la gestion comptable de ce type de retenu pour des marchés relativement faibles représente un coût qui souvent dépasse les 5% de la retenue.

## Garantie décennale

Nos travaux sont couverts par la garantie décennale. Précisons toutefois que, s’agissant des structures du bâtiment, ils sont conçus pour une tenue « à vie ». Pour cela, le bâtiment doit être normalement entretenu, les fuites d’eau doivent être réparées sans délais et aucune intervention ou accident ne doit affecter notre ouvrage.

Notre garantie porte en général sur les structures et non sur les parties accessoires existantes (parquet, tommette, etc). La garantie se limite à la zone d’intervention précisée au devis.

En cas litige, seuls les experts pourront déterminer les responsabilités exactes.

## Réserve de propriété

Les obligations contractuelles réciproques sont remplies dès lors que les travaux ont été exécutés (ou que le matériel a été installé), et que le client a versé intégralement le prix des prestations.

De convention expresse, les rapports, plans d'études, documents d’exécution ou note de calculs restent la propriété de DEBORD SAS, même s’ils sont communiqués à titre informatif.

Le défaut de paiement interdit tout transfert de propriété à des tiers de nos réalisations et rend abusive toute exploitation technique ou commerciale, qu'elle soit le fait du client ou de tiers.

En cas de fourniture de matériel, celui-ci reste la propriété exclusive de DEBORD SAS, quel que soit le détenteur, jusqu'au complet règlement de la facture par le client (loi 80 395 du 12.05.1980).

## Propriété industrielle

Les spécifications et informations techniques, modes opératoires, notes et programmes de calcul, procédés, appartenant à DEBORD SAS et issus des travaux, recherches et note de calculs effectués par nos soins, constituent son savoir-faire et doivent toujours être considérés par la personne à laquelle ils sont communiqués, à l'occasion d'un devis, rapport ou d'une consultation, comme strictement confidentiels et couverts par le secret professionnel.

En l’absence de contrat spécifique, toute utilisation des techniques développées par DEBORD SAS, le sont aux risques et périls de l’utilisateur, l’entreprise DEBORD SAS pourra en  réclamer des droits mais sa responsabilité ne pourra en aucun cas être recherchée.

## Environnement

L'entreprise s’engage pour le respect de l’environnement.

Elle s’engage dans la mesure des possibilités économiques, à minimiser l’impact en termes de pollution de l’air, de l’eau, de bruit et de poussière et assure l’évacuation des déchets en décharge publique spécialisée.


## Responsabilités

DEBORD SAS garantit que ses interventions sont conformes aux règlementations techniques en vigueur et sont réalisées selon les règles de l'art.

De convention expresse la responsabilité de DEBORD SAS est soumise aux limitations suivantes :

- la responsabilité de DEBORD SAS ne peut être retenue que dans les limites de la mission qui lui a été confiée et des éléments qui ont été portés à sa connaissance.
- La responsabilité de DEBORD SAS ne peut être recherchée pour des dommages ou modifications résultant d'erreurs, d'omissions ou d'imprécisions dans les documents remis par le client ou résultant d’impossibilité d’accès et de visualisation des éléments lors de la visite préalable.
- Les dispositions des Normes AFNOR P03-001 & P03-002 ne pourront être appliquées par défaut, sauf indication contraire formellement acceptées par DEBORD SAS.

En effet, ces dispositions sont difficilement applicables pour le type de travaux ponctuels que nous réalisons, apportant notamment une lourdeur de gestion administrative incompatible avec notre fonctionnement habituel et nos délais de réalisation.

## Assurance

DEBORD SAS assume, outre ses obligations contractuelles, la responsabilité civile et professionnelle de droit commun relative à ses prestations.

- Assurance responsabilité civile en cas d’accident survenu en cours de chantier. Attention, il ne s’agit pas des désordres inhérents à l’exécution des travaux qui sont pris en charge par le maitre d’ouvrage.
- Assurance professionnelle décennale pour les ouvrages réalisés, portant sur la fonctionnalité et la tenue de nos ouvrages. Cette assurance devient opérante lorsque les travaux sont terminés, dès la réception des travaux.

DEBORD SAS est garantie au titre de sa responsabilité civile et professionnelle auprès de :

SMABTP ALFORTVILLE – IMMEUBLE EQUALIA - CS 90003 - 5 RUE CHARLES DE GAULLE 94146 ALFORTVILLE CEDEX 

n° de contrat : 002953N1247000 / 001 289121/61.

## Conditions financières

Tous nos prix sont établis hors taxes ; ils sont majorés des taxes en vigueur, à la charge du client.

La T.V.A. est acquittée sur les encaissements.

Les règlements sont prévus comme suit :

- Acompte à la commande 30%
- Règlement sur situation intermédiaire pour les travaux dépassant un mois.
- Solde sur facture à la fin des travaux.

Toute somme non payée à l'échéance porte de plein droit à une indemnité forfaitaire de 40 euros (décret n°2012-1115 du 02/10/2012), à 1,5 % d’intérêts par mois ou fraction de mois, ainsi que tous autres frais de relance et poursuite ou préjudice.

Aucune mise en demeure n’est requise pour la mise en application de ces dispositions.

En cas de non-exécution des engagement pris par le client, DEBORD SAS peut en outre annuler tout ou partie de la commande, même après exécution partielle.


## Litige

Tout litige ou mécontentement de toute nature devra être immédiatement porté à la connaissance des responsables suivants, dans l’ordre, ceci afin d’espérer un règlement amiable au plus rapide:
    1. A l’équipe sur place dans le cadre de l’exécution du chantier
    2. A la direction de l’entreprise
    3. Aux forces de l’ordre en cas de conflit de voisinage.
    4. Au maître d’œuvre (ou architecte)
    5. Au maître d’ouvrage (syndic)
    6. A la justice.

## Attribution de juridiction

Dans toute contestation d'ordre contractuel, les tribunaux de Boulogne Billancourt seront seuls compétents.

## Droit à l’image - photos

Dans le cadre de ses prestations, l’entrepreneur peut être amené à réaliser des photographies pour un usage d’étude, de formation et de suivi de chantier.

Le client faisant appel à nous, donne automatiquement son consentement à notre personnel pour réaliser ces photos, ceci en son nom et au nom des personnes occupant les locaux auxquels nous aurons eu accès.

Il autorisera à la conservation des photographies et à leur utilisation dans le cadre de notre mission, des besoins de formation ainsi qu'à la promotion de l’entreprise à titre gracieux.

Elles pourront être insérées dans nos catalogues, dépliants, site internet et tout autre moyen de communication sans qu’il soit nécessaire d’autorisation préalable écrite du propriétaire ou occupant des lieux concernés.

Nous éviterons alors de fournir les éléments d’identification précise en cas de diffusion.

Concernant la diffusion de ces photos, nous nous engageons au respect du droit à l’image des personnes.
