---
title: "Les techniques de mise en charge"
description: "Passage en revue des différentes techniques de mise en charge."
date: 2020-06-28T21:52:37+01:00
draft: false
toc: true
featured_image: "/images/comprendre/Bordy - Techniques de mise en charge.webp"
show_reading_time: true
summary: "La mise en charge est l'une des clefs de voûte de notre savoir-faire. Mais concrètement, comment ça se passe sur le chantier ?"
categories:
- mise en charge
- notions
---

Maintenant que vous savez tout de la mise en charge grâce à [cet article](/comprendre/mise-en-charge/), vous vous demandez sûrement comment effectuer cette fameuse mise en charge. Alors passons en revue les différentes techniques !

## Les techniques standards de mise en charge

### Les mortiers

{{< figure src="/images/comprendre/charge05.webp" title="Certains mortiers ont des caractéristiques intéressantes" class="w-50-ns mr0 fr-ns mt0">}}

Les mortiers sans retrait (ou à retrait compensé) ne servent pas aux mises en charge, ils seront plutôt considérés comme un « blocage » sans affaissement. En effet, les mortiers ou bétons traditionnels se rétractent légèrement (diminution de volume) lors de la prise et du séchage. Ce phénomène, qui s’ajoute aux problèmes de tassement ou de flèche, amplifie les désordres, mais peut donc être éliminé grâce à ces mortiers. Nous vous conseillons [cet article sur les retraits du béton](http://www.explorations-architecturales.com/data/upload/interviews/fiche2.pdf) pour en savoir plus sur le sujet. (Si le lien d'origine ne fonctionne plus, [voici une copie](/pdf/fiche2.pdf)).

### Les plâtres

Ce matériau, au contraire, développe une légère expansion lors du durcissement, ce qui permet de réaliser un blocage serré.

Gâché sec, avec peu d'eau, il atteint une très grande dureté, équivalente à de la pierre. Cette mise en œuvre est appelée « plâtre à bâtir » et sert au montage des éléments structurels.

### Les coins

Ce système est le plus utilisé lors des reprises en sous-œuvre. Il consiste à enfoncer de force des coins au marteau pour mettre en charge les différentes pièces mises en œuvre.

L’effort appliqué est important, mais ne peut être mesuré directement. Toute la précision de ce travail réside dans la main qui tient le marteau, c’est à dire dans le savoir du compagnon, qui doit non seulement interpréter les charges à reprendre, mais aussi jauger l’effort réalisé par le coin, qui peut être colossal. En pratique, cela signifie qu'aucune mesure de mise en charge n’est généralement faite et que ce procédé reste d’un effet très aléatoire.

Il existe aussi une difficulté fréquemment rencontrée, à la source de plusieurs désordres : il est nécessaire de mettre en place deux coins face à face. S'il n'y en a qu'un, cela va engendrer des efforts parasites, sources de torsion des poutres ou de flambement des murs ou poteaux.

{{< figure src="/images/comprendre/charge07.webp" title="Le système à coins">}}

## Les techniques développées par Debord

Pour la réalisation de la mise en charge, il faudra non seulement créer un effort, mais aussi le mesurer et le contrôler. Pour cela, il faudra faire un choix parmi les nombreux procédés de forçage associés à des mesures.

### Le levier et contrepoids

Techniquement, c'est ce qu'il y a de plus simple : on créé une force avec un système de levier et un contrepoids. L’effort statique est maintenu le temps de l'opération.

Grâce au levier, on arrive à engendrer des forces importantes que l'on connaît précisément grâce à la masse connue du contrepoids, multipliée par le rapport de longueur du levier.

{{< figure src="/images/comprendre/charge60.webp" title="Mise en charge multiple sur la nouvelle poutre par leviers et contrepoids" class="w-50-ns center">}}
{{< figure src="/images/comprendre/charge61.webp" title="Solidarisation des arbalétriers sur la nouvelle poutre" class="doublepic fl-l">}}
{{< figure src="/images/comprendre/charge62.webp" title="Découpe des entraits" class="doublepic fr-l">}}

### Le vérin mécanique

{{< figure src="/images/comprendre/charge002.webp" title="Vérins en pieds de poteaux" class="w-50-ns mr0 fr-ns">}}

Nous utilisons couramment ce système pour la mise en charge des poteaux. Il consiste à mettre en place une tige filetée en tête ou en pied de poteau puis à serrer les écrous à l’aide d’une clé dynamométrique ou d’une clé à long manche avec peson de mesure du couple, pour appliquer la contrainte préalablement calculée. Il suffit ensuite de bloquer définitivement ce vérin par des plaques métalliques soudées ou du béton, le poteau finalisé devant pouvoir supporter une charge bien plus importante que le simple vérin.

Ces vérins mécaniques sont utilisés pour des charges inférieures à 6 tonnes.

La mise en charge à l’aide d’un étai sur notre dynamomètre de pied est aussi un système de vérinage.

#### En bout

Lorsqu'on utilise un vérin en bout de poutre, la force est créée au droit d’un appui. Elle est multipliée au centre par effet de levier.

{{< figure src="/images/comprendre/charge0051.webp" title="Schéma" >}}

Dans ce cas de figure, la poutre repose sur son appui à une extrémité seulement, puis est fixée au centre de la poutrelle à renforcer. Ensuite, on monte l’autre extrémité à la moitié de la charge calculée pour le centre, puis on effectue le scellement ou le calage. Le dispositif de mise en charge peut être retiré après blocage et scellement.

#### Au centre

Sur le schéma ci-dessous, la traction est réalisée au centre de la poutrelle mise en place, en s’appuyant sous le plancher à conforter. La poutre repose sur ses appuis à ses deux extrémités. La charge est contrôlée à la clef dynamométrique et/ou par mesure de la flèche qui est parfois plus précise. Une fois la poutrelle fixée sur toute sa longueur, le dispositif de mise en charge peut être retiré.

{{< figure src="/images/comprendre/charge005.webp" title="Tendeur ou vérin de flèche au centre de la poutre" >}}

### La tige filetée
La tige filetée permet d’exercer une traction contrôlée, connaissant la force exercée en fonction du couple de serrage engendré à la clé dynamométrique.

Ce système peut être utilisé sur une poutre horizontale. Simple à mettre en œuvre, ce procédé est fréquemment utilisé sur le moisage de poutre ou le renfort unilatéral.
L’avantage de ce système, outre sa précision, est que l’effort de traction est créé par appui négatif directement sous le plancher ou le mur à conforter : l’équilibre est immédiat et il n’y a aucun effort engendré sur le plancher bas comme avec le système de vérin en bout de poutre.

{{< figure src="/images/comprendre/charge004.webp" title="Tendeur de flèche sur renfort unilatéral" >}}

### Le coin-écarteur

Toujours sur le principe du boulon, et pour des petites charges, le coin-écarteur est plus facile à installer sans perçage. Le serrage se fait à la clé dynamométrique.

{{< figure src="/images/comprendre/charge64.webp" title="Coin écarteur de mise en charge à vérin" >}}


### Le vérin hydraulique

Ce vérin développe en général une force plus importante : nous en utilisons jusqu’à 50 tonnes et nous les abandonnons habituellement dans la construction après blocage.
Ce système est plus précis car la mesure se fait au peson sur le bras de pompage, dont la force est proportionnelle à la pression d’huile et donc à l’effort transmis par le piston. Le vérin mécanique est un peu moins précis de par l’existence de frottements importants que l’on n’a pas ici.


### Le chauffage

Ce procédé particulier ne peut s’appliquer que dans des cas très restreints de renfort par fer collaborant, solidarisé à une poutre bois trop faible par exemple. Il consiste à chauffer la pièce métallique, qui se dilate, avant de la fixer. En refroidissant, le fer se rétracte et provoque une contrainte : la mise en charge est faite.
Seules les pièces métalliques peuvent être chauffées de façon contrôlée pour obtenir un allongement ou un rétrécissement déterminés.

{{< figure src="/images/comprendre/charge09.webp" title="Principe de fonctionnement de la mise en charge par chauffage" >}}

> La dilatation de l’acier étant connue (11 × 10- 6 K-1 pour les aciers couramment utilisés en construction), il suffit de chauffer le métal à 80 degrés par exemple pour obtenir un allongement de 2,6 millimètres sur un fer de 4 mètres de long.

Le procédé consiste à fixer les éléments entre eux une fois le fer chauffé et dilaté. Au refroidissement, en se  rétractant, il se créera une poussée vers le haut en réaction.

Avantage du système par fer collaborant : le bois en partie haute est comprimé et le fer en partie basse est tendu: une poutre ainsi renforcée voit son processus de rupture modifié dans le sens de la sécurité, car la rupture – si elle se produit – n’est pas brutale, mais progressive.

En effet, un élément en bois sans renfort aura une rupture rapide : les fibres tendues (celles en partie basse de la poutre) vont se rompre les unes après les autres. Plus il y aura de fibres cassées, moins il y aura de fibres de maintien et donc plus la rupture s’accélère. À l'inverse, une poutre suffisamment renforcée par un élément métallique va se rompre par écrasement des fibres hautes du bois. Il n’y a donc pas d’effet exponentiel.

Nous mettons en œuvre ce système de mise en charge lors de confortement d’une poutre ou solive bois, en encastrant un fer en partie inférieure pour préserver son aspect lorsqu’elle est destinée à rester apparente.

Le fer peut ne pas être encastré lorsqu’on accepte une retombée supplémentaire et que l’on veut conserver un bois existant, pour ouvrir une baie par exemple.
