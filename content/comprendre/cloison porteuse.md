---
title: "Cloison porteuse"
description: "De la théorie à la pratique"
date: 2020-03-01T06:40:02+01:00
draft: false
toc: true
featured_image: "/images/comprendre/Bordy - Cloison porteuse.webp"
show_reading_time: true
summary: "Parfois, il nous prend l'envie d'abattre des murs. Mais l'immeuble va-t-il nous tomber dessus ? Il s'agit bien d'une cloison toute fine, peut-elle être porteuse ?"
categories:
- notions
---

## Porteur ou pas, comment savoir ?

De façon générale, on a tendance à penser que si une paroi fait moins de 8 cm, alors c'est une cloison, par définition non-porteuse. Malheureusement ce n’est que la théorie !

>Une cloison est une paroi entre deux pièces, destinée à réaliser une séparation physique, phonique, visuelle, voire thermique ou olfactive. Elle n’est pas destinée à être porteuse du bâtiment, des planchers ou de tout élément de bâtiment des étages supérieurs.
>Et pourtant, il arrive qu’elle le soit devenue. Dans ce cas, des désordres apparaissent lorsqu’on la supprime sans précaution.

### Habitation récente

{{< figure src="/images/comprendre/cloison_21.jpg" alt="exemple d'immeuble récent" class="fl-ns w-50-ns mt0 ml0" >}}

Sauf construction particulière, pour les bâtiments récents (après 1960) la théorie et la pratique se rejoignent. Il existe donc peu de problèmes, ces cloisons peuvent être supprimées sans grand risque de désordres.

### Habitation ancienne

{{< figure src="/images/comprendre/cloison_22.jpg" alt="exemple d'immeuble ancien" class="fr-ns w-50-ns mt0 mr0" >}}

Mais dès qu'on passe à l'ancien, il existe de nombreux cas de figure assez complexes et la prudence est de mise.

**Dans les vieux immeubles, les cloisons doivent être considérées comme porteuses, _a priori_.**

## Que faire en cas de doute ?

Plusieurs éléments peuvent nous alerter :
* Le plafond (ou le plancher) n’est pas plan, pas de niveau
* Il existe la même cloison à tous les étages
* Il y a déjà des fissures ou des traces de désordres
* La paroi est bombée

Quoi qu'il en soit, vous avez deux approches possibles :

### Vérifier l’état et le dimensionnement du plancher
Cela nécessite une étude avec souvent des sondages destructifs. Les sondages servent à déterminer la structure du plancher pour pouvoir la recalculer (portée de chaque élément, composition, disposition, section, état) afin de connaître la capacité théorique du plancher et la comparer aux charges qu’il a à supporter. Le résultat est souvent sans ambiguïté.
Attention cependant, si ces calculs indiquent que le plancher est bien apte à reprendre les charges, il est possible qu’il repose malgré tout, ne serait-ce que partiellement, sur la cloison inférieure, ce qui fait qu’en la démolissant, le plancher ne s'écroulera pas, mais s’affaissera légèrement lorsqu'il reprendra totalement la charge. Cela risque de créer quelques fissures dans les cloisons au-dessus, qu’il suffira alors de reboucher si besoin.
Notons que s’il n’existe pas de cloisons au-dessus, la descente de quelques millimètres de ce plancher n’aura aucune conséquence visible.

### Remplacer la cloison par une poutre par précaution
Afin d’éviter les sondages destructifs ainsi que le risque d’affaissement décrit ci-dessus, on peut faire d'une pierre deux coups en installant un renfort. La poutre devra alors être calculée et dimensionnée pour reprendre la totalité des charges, en considérant que le plancher devra pouvoir s’y appuyer totalement. Notons que pour éviter les risques d’affaissement, cette poutre devra être [mise en charge](/comprendre/mise-en-charge) avant la démolition de la cloison, pour éviter justement le petit affaissement prévisible. L’inconvénient de ce choix « par précaution » est que nous installons une poutre qui pourra être gênante à l'avenir alors qu’elle n’est peut-être pas nécessaire. Et personne n’osera plus l’enlever…

## Responsabilité

Quoi quoi quoi ?! J'ai créé des fissures ? Une cloison ne devrait pas être porteuse, en quoi serais-je responsable ? Pour les assurances, lorsqu’il y a un problème, ce sont vos travaux, vos reprises en sous-œuvre ou ici la suppression de la cloison qui est généralement considérée comme « fait générateur du désordre ».

Dans un immeuble collectif en copropriété, le copropriétaire de son appartement est généralement en droit (voir règlement de copro) de modifier son aménagement intérieur. Il peut démolir ou refaire le cloisonnement par exemple, tant que ses travaux n’ont pas d’impact ou ne créent pas de nuisances ou de désordres structurels dans le bâtiment.

Par contre, s’il envisage de modifier un mur porteur, il doit en demander l’autorisation à la copropriété et assurer la stabilité de l’immeuble pendant et après les travaux.

Une cloison, si elle est porteuse, doit donc être considérée comme un mur porteur car sa suppression peut provoquer un affaissement ou un effondrement des planchers. Ceci est dû à la souplesse des planchers, à leur sous-dimensionnement originel, à leur surcharge ou à leur dégradation. Si le plancher ne semble pas bouger au moment de la démolition de la cloison, des désordres peuvent apparaitre plus tard dans les étages supérieurs, avec des fissurations dans les cloisons, des portes qui se coincent, des sols qui bougent, etc… Ces désordres peuvent apparaitre longtemps après cette suppression, de quelques jours à des années après, les désordres se propageant doucement dans les étages supérieurs. Oui, car votre maison est [visqueuse](/comprendre/mise-en-charge/#la-viscosité-des-bâtiments).

Même si, à la base, la cloison ne devrait pas être porteuse et qu’elle l’est devenue à la suite d’une défaillance quelconque, il faut savoir qu’en cas de désordre suite à sa démolition « sans précaution », c’est bien la personne qui supprime la cloison qui sera considérée responsable des fissures et désordres aux étages supérieurs. Cependant, en cas de défaillance du plancher, par dégradation dues à des infiltrations par exemple, la réparation du plancher relève de la responsabilité de la copropriété, qui pourra se retourner contre la personne responsable des fuites (si sa responsabilité peut être établie). En général, les désordres sont anciens et il est difficile de faire intervenir les assurances, la copropriété devant réparer son plancher à ses frais. Les assurances se retranchent souvent derrière le prétexte qu’un accident doit être ponctuel, déclaré dans les 48h et opposent souvent qu’il s’agit plutôt de vétusté, de négligence, de défaut d’entretien, etc… Elles prennent en charge la réfection des peintures et embellissements s’il y a un dégât déclaré, et parfois un petit pourcentage de la réparation structurelle (15% par exemple).
L’idéal serait de réparer la défaillance du plancher elle-même, avant de supprimer la cloison, et avant que le désordre ne se produise.

>Bien que la réparation du plancher relève de la copropriété, la personne voulant supprimer la cloison propose parfois de participer aux frais. Elle garde souvent à sa charge la réfection des embellissements.

## Que faire en cas de désordres ?

Si à la suite de la démolition d’une paroi, des fissures sont apparues, il convient alors de vérifier si la structure de ce plancher est apte à recevoir les charges, avec sondages éventuels et recalcul de la structure.

### Étude de cas
Nous effectuons une étude visant à calculer si le plancher est apte à recevoir toutes les charges qui lui sont appliquées ou sont susceptibles de l'être, car il faut compter les charges d’exploitations générées par l'occupation normale du local (mettre un piano ou une bibliothèque par exemple). Ces charges sont définies par la norme française NF P 06-001 selon l’usage du plancher considéré (habitation, grenier, balcon, escalier, bureau, commerce, etc…)
Deux conclusions sont possibles :

#### 1 – Le plancher n'est pas apte à recevoir les charges
Dans ce cas, le plancher est considéré comme instable, un confortement est nécessaire et un étaiement provisoire doit être réalisé dans cette attente.

#### 2 – Le plancher est apte à recevoir les charges
Dans ce cas, les mouvements doivent s’arrêter rapidement, le plancher est considéré comme stable et les désordres ne sont pas évolutifs.
Il suffit en général de remettre en jeux les portes ou de reprendre les peintures sur les fissures. Ces travaux sont à la charge du maître d’ouvrage, celui qui a commandé les travaux de démolition de la cloison considérée. Il profite en général de la présence sur place de son propre peintre qui réalisera ces reprises en fin de travaux, lorsque tout est bien stabilisé.
Notons que dans tous travaux de démolition ou de reprise en sous-œuvre, le risque de fissuration existe, il est même souvent considéré comme « normal », consécutifs aux travaux. Les assureurs refusent alors de les considérer comme des désordres accidentels. Leur réparation doit donc faire partie de ces travaux et être pris en charge par le maître d’ouvrage.
