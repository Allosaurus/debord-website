---
title: "Mur bouffant"
description: "Il peut arriver qu’un mur en pierre veuille s’effondrer sur lui-même, sans que l'on soit forcément d'accord. Quelles sont les solutions face à ce problème ?"
date: 2024-08-29T14:32:02+02:00
draft: false
featured_image: /images/comprendre/bordy_mur_bouf_01.webp
show_reading_time: true
toc: true
summary: "Il peut arriver qu’un mur en pierre veuille s’effondrer sur lui-même, sans que l'on soit forcément d'accord. Quelles sont les solutions face à ce problème ?"
categories:
- notions
- mur bouffant
- flambement
- affaissement de mur
- mur ventru
- bouffement
- effondrement
---

## Le phénomène

<div class="imgandcaption">

Il arrive qu’un mur en moellons de pierre s'affaisse sur lui-même. Il commence par s’affaisser et gonfler avant de se désagréger puis de s’écrouler. L’évolution peut être très lente au début, puis elle s’accélère, et même très rapidement sur la fin. Pour parler de ce phénomène d'affaissement de mur, on utilise plusieurs termes plus ou moins synonymes : mur ventru, flambement, bouffement, effondrement… ou mur bouffant.

{{< figure src="/images/comprendre/mur_bouffant_14.webp" title="Effondrement en cours, on voit la progression des fissures vers le haut" class="mr0">}}

</div>

<div class="flex flex-wrap">

{{< figure src="/images/comprendre/mur_bouffant_15.webp" title="Mur bouffant avec ventre de 16 centimètres" class="w-48-l center" >}}

{{< figure src="/images/comprendre/mur_bouffant_16.webp" title="Effondrement des deux parements" class="w-48-l center" >}}

</div>

{{< figure src="/images/comprendre/mur_bouffant_17.webp" title="Effondrement en cours, seul le premier parement est tombé" >}}

>Certaines photos proviennent du site [Tiez Breiz – Maisons et Paysages de Bretagne](https://www.tiez-breiz.bzh), avec l'aimable autorisation de cette association spécialisée dans le patrimoine rural breton.

## Les causes

Ce phénomène peut être dû :

* À un défaut d’appareillage (défaut de fabrication)
* À une surcharge
* À un dégât des eaux
* À un peu de tout ça en même temps

### Défaut d'appareillage

Avant d’aller plus loin, il est nécessaire de comprendre comment est construit un mur en pierre, comment il est "appareillé".

Il est extrêmement rare de pouvoir disposer de pierres naturelles qui fassent exactement l’épaisseur du mur, à moins de les tailler spécialement (on parle alors de pierre de taille).

Un mur en moellons de pierre est donc constitué de deux parements, c’est-à-dire que les pierres sont choisies ou taillées avec au moins une face plate qu’on alignera convenablement d’un côté et qui constituera une face du mur. Sur la seconde face, on fait pareil et on obtient la deuxième face apparente. Au milieu, on dispose des pierres de remplissage qui ne sont pas forcément ni plates ni bien taillées.

Si on en reste là, le mur ne supportera pas de grosses charges : le milieu va s’écraser en poussant et en écartant les deux parements. Il va s’affaisser, devenir « ventru ». On parlera alors de « mur bouffant ».

Pour éviter cela, il faut disposer à l’intérieur du mur des pierres longues qui vont faire la liaison entre les deux parements pour maintenir l’écartement et créer la cohésion de l'ensemble. On les appelle les boutisses et les parpaings.

>Lorsqu'une pierre relie le milieu du mur à un parement, on l'appelle « boutisse ».

>Mieux encore, lorsqu'une pierre traverse tout le mur avec deux faces apparentes plates, on l'appelle un « parpaing » (du latin « perpetaneus » qui signifie ininterrompu). C’est d’ailleurs cette forme de pierre taillée bien carrée, parallélépipédique, qui a donné leur nom aux parpaings en béton préfabriqués du commerce.

S’il n’y a pas suffisamment de boutisses ou de parpaings, le mur est fragile, instable et s’affaisse sur lui-même.

<div class="flex flex-wrap">

{{< figure src="/images/comprendre/mur_bouffant_dessin01a.webp" title="Mur en pierre : appareillage des moellons" class="w-48-l center" >}}

{{< figure src="/images/comprendre/mur_bouffant_dessin01b.webp" title="Affaissement d'un mur en pierre : mur bouffant, mur ventru" class="w-48-l center" >}}

</div>

### Mur surchargé

Il est fréquent qu’un mur convenablement construit soit par la suite surchargé. C’est par exemple le cas lorsqu’on construit un immeuble sur un mur existant qui n’était au départ qu’un mur de clôture, ou lorsqu’on surélève de deux ou trois étages un immeuble qui n’avait initialement que quatre étages.

## Méthodes de réparation

### Démolition et reconstruction

C’est encore le plus simple si le mur n’est pas trop haut et s’il n’est pas surchargé.

### Butonnage, contreforts et moisage

Ces différentes méthodes consistent à maintenir la cohésion du mur par des pressions extérieures.

Ce sont des opérations qui sont souvent moins chères du fait qu’il n’y a pas de reprise en sous-œuvre, mais elles ont l’inconvénient de prendre de la place en-dehors du mur.

#### Butonnages

Les butonnages sont considérés comme des étaiements provisoires.

<div class="flex flex-wrap">

{{< figure src="/images/comprendre/mur_bouffant_18.webp" title="Butonnage sur 8 mètres de haut" class="w-48-l center" >}}

{{< figure src="/images/comprendre/mur_bouffant_11.webp" title="Butonnage intérieur au rez-de-chaussée. Ventre de 17 centimètres" class="w-48-l center" >}}

</div>

#### Contreforts

<div class="flex flex-wrap">

{{< figure src="/images/comprendre/mur_bouffant_dessin02.webp" title="Contrefort en béton armé avec semelle enterrée" class="w-48-l center" >}}

{{< figure src="/images/comprendre/mur_bouffant_03.webp" class="w-48-l center" >}}

</div>

#### Moisage ou frettage

<div class="flex flex-wrap">

<div class="flex flex-column w-48-l center">

Avec cette solution, le mur est pris en sandwich entre des profilés métalliques maintenus par des tiges filetées traversantes.

L’intervention impacte les deux côtés du mur. Les fers devront être protégés contre la rouille et contre le feu.

{{< figure src="/images/comprendre/mur_bouffant_dessin04.webp" title="Croquis de principe, vue en plan" class="center" >}}

</div>

{{< figure src="/images/comprendre/mur_bouffant_dessin03.webp" title="Coupe de principe" class="w-30-l center" >}}

</div>

<div class="flex flex-wrap">

{{< figure src="/images/comprendre/mur_bouffant_06.webp" title="Moises métalliques" class="w-40-l center" >}}

{{< figure src="/images/comprendre/mur_bouffant_19.webp" title="Moises avec plâtre coupe-feu" class="w-40-l center" >}}

</div>

Ces solutions sont certes efficaces, mais elles ne brillent pas par leur discrétion… Heureusement, cet article n'est pas fini.

### Insertion de raidisseurs en béton armé

Cette méthode consiste à réaliser des saignées verticales dans l’épaisseur du mur, dans lesquelles le béton est coulé à même les pierres. Des goujons métalliques scellés à la résine époxy dans les pierres complètent la cohésion.

À lui seul, le poteau raidisseur en béton armé ainsi créé pourrait reprendre toutes les charges du mur, et il fait corps avec lui.

{{< figure src="/images/comprendre/mur_bouffant_dessin05.webp" title="La cohésion entre poteau et mur est totale" >}}

Le mur étant fragile, il devra être sécurisé avant la réalisation des saignées, et les différents raidisseurs devront être réalisés les uns après les autres. Le coulage du béton doit également se faire par étapes, car le mur sert de coffrage et pourrait éclater sous la pression du béton.

Il s’agit d’une reprise en sous-œuvre.

#### Confortement d'un mur pignon

<div class="flex flex-wrap">

{{< figure src="/images/comprendre/mur_bouffant_01.webp" title="Création de la saignée pour un tronçon supplémentaire" class="w-48-l center" >}}

{{< figure src="/images/comprendre/mur_bouffant_02.webp" title="Tronçon ferraillé avant mise en place des agrafes dans les pierres" class="w-48-l center" >}}

</div>

<div class="flex flex-wrap">

{{< figure src="/images/comprendre/mur_bouffant_05.webp" title="Le béton est coulé au fur et à mesure" class="w-48-l center" >}}

{{< figure src="/images/comprendre/mur_bouffant_20.webp" title="On peut voir les cinq passes de bétonnage" class="w-48-l center" >}}

</div>

>Retrouvez un exemple d'intervention de ce type [dans nos réalisations](/realisations/bertin-poiree).

#### Mur de cave renforcé

Dans l'exemple précédent, le béton reste apparent, mais il est bien sûr possible d'enduire le mur pour plus de discrétion, comme ci-dessous.

{{< figure src="/images/comprendre/mur_bouffant_12.webp" title="On distingue encore les « fantômes » des raidisseurs béton" class="center" >}}

### Insertion de raidisseurs en acier

Il existe une autre solution, qui consiste à utiliser des raidisseurs en acier. Comme pour les raidisseurs en béton armé, les poteaux sont insérés dans l’épaisseur du mur, en accédant d’un seul côté.

L’avantage de cette méthode est qu’elle permet la [mise en charge](/comprendre/mise-en-charge) de ces poteaux.

En effet, si l’affaissement du mur a commencé, il se poursuivra progressivement à tous les étages au cours des années suivantes, même si le mur est stabilisé… À moins de mettre en charge les poteaux afin qu’ils reprennent immédiatement la charge du mur, avant que les étages supérieurs ne descendent.

Pour une bonne reprise des charges sur nos poteaux, il est nécessaire de réaliser préalablement des sommiers hauts et bas en béton pour répartir les efforts sur les maçonneries existantes. Ensuite, les poteaux en acier relient ces deux sommiers.

Un autre avantage sur les poteaux béton est leur faible emprise. Cela permet de reconstituer le parement devant les poteaux, qui pourront ainsi être totalement noyés. Si ce parement est trop endommagé, il est possible, une fois tous les poteaux installés et mis en charge, de le démonter complètement et de le remonter à neuf.

Afin de redonner une cohésion au mur, on liaisonne les deux parements à l’aide de goujons scellés dans les pierres du second parement (celui que l’on n’a pas touché) et qui seront noyés dans notre nouvelle maçonnerie du premier parement.

{{< figure src="/images/comprendre/mur_bouffant_dessin06.webp" title="Croquis de principe" class="center" >}}

La méthodologie est la suivante :

* Étude et calcul des charges à reprendre et dimensionnement des poteaux.
* Butonnage du mur et étaiement des planchers avant intervention.
* Démontage des pierres en haut et en bas et coulage des sommiers en béton armé.
* Réalisation d’une saignée verticale entre ces sommiers et insertion du premier poteau.
* Mise en charge à l’aide d’un vérin noyé.
* Remontage et blocage des pierres (à moins que le parement ne soit refait dans sa totalité après-coup).
* Réalisation du poteau suivant selon la même méthode.

Ces interventions doivent se faire les unes après les autres ou en alternant un poteau sur deux.

{{< figure src="/images/comprendre/mur_bouffant_21.webp" title="Mise en place des sommiers et des poteaux" class="center" >}}

{{< figure src="/images/comprendre/mur_bouffant_dessin11.webp" title="Remontage du parement" class="center" >}}

{{< figure src="/images/comprendre/mur_bouffant_23.webp" title="Le mur renforcé et reconstitué est prêt à recevoir les enduits et rejointoiements de finition" class="center" >}}

{{< figure src="/images/comprendre/mur_bouffant_24.webp" title="Le second parement n'est pas touché par l'opération : il reste intact" class="center" >}}


## Conclusion

Normalement, un mur en pierre est construit bien vertical, bien plan.

S’il présente un « ventre », une bosse, c’est qu’il s’est produit un affaissement. Soit dès la construction et le phénomène évolue lentement, soit déclenché par une surcharge postérieure ou un dégât des eaux, l’eau servant de lubrifiant qui diminue l’adhérence des pierres et la solidité du mortier. Le tout est de déterminer si c’est ancien et stabilisé (on peut toujours rêver) et si ça ne bouge plus.

En effet, cette déformation peut être très ancienne, mais s’il y a des fissures récentes, c’est que l’évolution est toujours en cours et que ça peut s’accélérer. Par exemple, si le mur a 200 ans, qu’il n’y a pas eu de confortement récent, et que la peinture fissurée à 20 ans, on considère que le désordre est évolutif. Un confortement est nécessaire.

Trois techniques de confortement sont radicales et permettent de multiplier par deux ou trois la capacité portante de ces vieux murs en pierre et de les stabiliser définitivement tout en les conservant.

<div class="flex justify-center flex-wrap">

  {{< figure src="/images/comprendre/mur_bouffant_dessin08.webp" title="Moisage" class="ma1 w-30-l w-40-m">}}
  {{< figure src="/images/comprendre/mur_bouffant_dessin09.webp" title="Raidisseur béton" class="ma1 w-30-l w-40-m">}}
  {{< figure src="/images/comprendre/mur_bouffant_dessin10.webp" title="Raidisseur métallique" class="ma1 w-30-l w-40-m">}}

</div>
