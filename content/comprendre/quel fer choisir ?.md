---
title: "Quel fer choisir ?"
description: "IPN ou autres, les différents types de fer"
date: 2020-03-19T11:51:09+01:00
draft: false
featured_image: "/images/comprendre/Bordy - Quel fers choisir.webp"
toc: true
show_reading_time: true
summary: "On parle souvent de mettre un IPN. Mais il n'y a pas que les IPN dans la vie. Il y a aussi les HEA, HEB, les IPE ou autres UPE."
categories:
- matériaux
---

## Quel faire ? Pour quoi fer ?

### Qu'est-ce qu'un fer ?

Les « fers » utilisés dans la construction sont des profilés métalliques préfabriqués de sections et dimensions diverses, mais de même profil sur toute leur longueur. Ils sont fabriqués par [laminage](https://fr.wikipedia.org/wiki/Laminage) ou [filage](https://fr.wikipedia.org/wiki/Filage_(m%C3%A9tallurgie)).

### À quoi ça sert ?

Ils servent à être assemblés sur place pour constituer ou participer à la structure du bâtiment, résister à des efforts statiques ou dynamiques, et assurer sa stabilité. Ils constituent les poteaux, poutres, solices, chevêtres, chaînages, écharpes, etc.

### Comment ça marche ?

Le métal a la particularité de résister dans toutes les directions où il est sollicité, que ce soit en compression ou en traction. On peut aussi lui donner la forme que l’on veut, ce qui permet de choisir précisément sa résistance à tel ou tel endroit ou dans tel ou tel sens.

> #### Exemple d'une poutre
>Sur une poutre en charge, la partie supérieure est comprimée, tandis que la partie inférieure est tendue. On emploie le même matériau, fer ou acier, pour ces deux parties et, pour être efficace et diminuer les efforts, on les écarte en les maintenant solidaires par la partie centrale : l'âme. On concentre donc le maximum de fer en haut et en bas, juste reliés entre eux. Ceci nous donne une poutre au profil très efficace, en forme de I. Elle est pratique, résistante et très légère comparée au bois ou la pierre. De plus, on peut l’employer un peu partout comme poutre, solive, linteau, raidisseur, etc.
Seul inconvénient : on ne peut pas faire varier le profil sur la longueur du fer, c’est le même partout.
Or, dans une poutre par exemple, on a besoin du maximum de résistance au centre, et on pourrait donc avoir beaucoup moins de matière sur les bords. Il reste la possibilité de fabriquer des poutres sur mesure, par exemple pour les ponts et ouvrages d’art, ou en réalisant des assemblages de différents profilés.

## Les différents types de fer

Il existe une multitude d'éléments standards, préfabriqués, d’un emploi extrêmement pratique. Les formes différentes ont un domaine de prédilection différent, avec bien sûr à chaque fois une déclinaison en de multiples dimensions. En charpente, on utilise beaucoup les fers de type I, H, U, les cornières, les tubes ronds, carrés, rectangulaires ou même les fers pleins carrés ou rectangulaires, les fers plats et les fers en T.

{{< figure src="/images/comprendre/Sectionsfers.webp">}}

### Les fers en I

C’est la forme optimisée pour les poutres, en utilisant le moins de métal possible, à la condition qu’il soit placé « debout ». Aux débuts historiques du laminage de ces profilés, chaque usine fabriquait ses propres fers avec ses propres dimensions, avant qu'elles ne soient normalisées. Voici les principaux :

#### IAO (I à aile ordinaire)

Apparu en 1845, il se retrouve dans les constructions anciennes. Le fer de type IAO a été le premier à être employé à grande échelle, et nous le rencontrons dans les anciennes constructions du 19e siècle. C'est lui qu'on retrouve majoritairement dans les planchers des immeubles haussmanniens.

#### IPN (I à profil normal)

Le fer de type IPN, arrivé autour de 1940, s’est imposé en remplacement des IAO. Un peu plus large (la moitié de sa hauteur), c’est un profilé plus stable, moins sensible au vrillage et au déversement que l’IAO. Il constitue l'élément principal dans la reconstruction des immeubles d'après-guerre. Les grosses sections ont été abandonnées à la fin du 20e siècle, mais il est toujours utilisé pour les petites tailles (jusqu'à 200mm).

#### IPE (I à profil européen)

Le fer de type IPE est plus récent (années 1960). Ses angles sont plus marqués, moins arrondis que sur l’IPN, et ses faces sont parallèles. Avantage : les assemblages sont plus faciles à réaliser à l’intérieur des ailes et son rapport résistance/poids est amélioré. Ceci au léger détriment du rapport résistance/hauteur du fer.

### Le fer en U

Les plus utilisés sont l'UPN, l'UPE et l'UAP (qui n’est plus fabriqué mais souvent présent dans l’existant). Il sont très utiles pour des utilisations dissymétriques ou pour réaliser des assemblages.

### Les fers en H

Ces fers sont beaucoup plus larges que les I : leur hauteur est proche de leur largeur. Ils sont moins intéressants en termes de résistance pure s’ils sont utilisés en poutre ou dans un plancher, avec un moins bon rapport résistance/poids, mais ils ont un module de résistance moins dépendant du sens de pose que les fers à I, donc plus stables utilisés seuls sans entretoises, et comme poteau.
Les plus utilisés actuellement sont les HEA et HEB. Le poids et la résistance peuvent être très différents d'un type de H à l'autre et pour une même hauteur. Le rapport résistance/poids est bien meilleur sur les HEA que sur les HEM. Certes, ces derniers portent plus, mais ils sont très, très, mais vraiment très très lourds. (Très)

### Les autres fers

Concernant les autres fers tubes et fers pleins, la « standardisation » est beaucoup moins nette. Nous rencontrons fréquemment des fers de formes et d’épaisseurs différentes chez nos fournisseurs ou sur nos chantiers, où on peut trouver des fers anciens de toutes sortes, tels que d'anciens rails de chemin de fer réutilisés en structure de bâtiment.

## De la théorie à la pratique

### La théorie

Tous les types de fers peuvent être utilisés pour tous les types d’effort, à condition que le calcul soit vérifié.
On peut jouer sur la section. Par exemple, nous avons dit que le fer IPN devait être posé « debout ». Cependant, il pourra être posé « couché », à condition d’adapter la dimension. Un IPE de 180 mm debout pourra donc être remplacé par un IPE de 400 mm à plat. Il aura la même hauteur (18 cm), mais sera quatre fois plus large et quatre fois plus lourd ! Pas grand intérêt.
On peut aussi utiliser des types de fers différents, toujours à la condition de les vérifier par le calcul. Par exemple, un HEB de 180 mm aura une résistance similaire à un IPE de 300 mm. Il sera presque moitié moins haut (18 cm au lieu de 30 cm), ce qui devient intéressant, mais il sera 42% plus lourd pour la même portée.

### La pratique

De façon générale, il sera plus économique d’utiliser :
- Les H pour les poteaux
- Les I pour les poutres (ou des H si la hauteur doit être restreinte)
- Des poutres assemblées composées de divers profilés de sections et de longueurs différentes, plus complexes mais plus intéressantes.

Ensuite, tout sera une affaire de comparaison et de priorités : poids ou hauteur ? Ces deux éléments étant souvent les plus importants pour des problèmes d’encombrement, de coût et de difficultés de manutention. La disponibilité des fers a aussi son importance en pratique.
