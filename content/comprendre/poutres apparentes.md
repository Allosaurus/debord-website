---
title: "Poutres apparentes"
description: "Un choix qui n'est pas sans conséquences"
date: 2021-06-08T18:04:29+02:00
draft: false
featured_image: "/images/comprendre/Bordy - Poutres apparentes.webp"
show_reading_time: true
toc: true
summary: "Mur en pan de bois et plafonds à poutres apparentes donnent beaucoup de cachet à une pièce. Mais cette mise à nu n'est pas aussi anodine qu'il n'y paraît…"
categories:
- poutre
- pan de bois
- stabilité au feu
- coupe-feu
---

## Poutres et pans de bois à Paris

<div class="imgandcaption">

La plupart des immeubles centenaires de Paris ont été construits avec des poutres en bois. Malheureusement, ces éléments sont la plupart du temps cachés, alors que beaucoup préfèreraient les mettre en valeur.

{{< figure src="/images/comprendre/poutreapp.webp" class="mr0">}}

</div>

{{< figure src="/images/realisations/refend/ouverturerefend01.webp">}}

>Murs en pan de bois et plafonds à poutres apparentes donnent un « cachet » ancien très recherché.

## Peut-on rendre ses poutres apparentes ?

**Oui !**

Il y a toutefois plusieurs conditions à respecter, car loin d'être une simple question d'esthétique, le fait de rendre apparente une structure bois modifie quelques-unes des caractéristiques fondamentales d'un bâtiment.

### Légalement

En copropriété, c’est le règlement de copropriété qui définit si le plafond plâtre vous appartient ou s'il constitue une « partie commune » régie par la copropriété.

Dans ce dernier cas, vous devrez obtenir l’accord de votre copropriété avant toute intervention. Mais vous ne pourrez de toute façon pas faire n'importe quoi.

### Comportement de la structure en cas d'incendie

#### Tenue de l’immeuble pendant l’incendie : la stabilité au feu

La norme impose aux immeubles d’habitation une certaine résistance au feu en cas d’incendie, ceci afin de permettre l’intervention des pompiers et l’évacuation du bâtiment avant un éventuel effondrement. En général, pour les immeubles de plus de deux étages, ce temps de résistance doit être d’au moins une heure.

Cette résistance au feu est en bonne partie assurée par le plâtre, matériau incombustible, qui protège les différents éléments de structure.

>C’est à la suite du grand incendie de Londres en 1666 que Louis XIV a imposé que tous les bois des immeubles parisiens soient recouverts de plâtre.

Supprimer le plafond plâtre expose donc les poutraisons à l’incendie. 

C'est pourquoi avant d'envisager de tels travaux, il faut s'assurer que la structure ainsi mise à nu résistera au moins une heure.

Pour le vérifier, nous simulons par le calcul un incendie et imaginons l’état des différentes poutres après une heure d’incendie. Cela nous permet de voir si les sections de bois restantes sont suffisamment porteuses, ou si tout se serait effondré.

{{< figure src="/images/comprendre/poutresincendie.webp" title="Coupe d'un plafond en poutres apparentes après une heure d'incendie">}}

Dans ce deuxième cas, des dispositions particulières sont possibles : nous mettons en place des renforts complémentaires garantissant une bonne tenue au feu.

#### Propagation du feu vers les voisins : le coupe-feu
    
Un coupe-feu d’une heure doit également être respecté. Cela signifie que ni le feu, ni la chaleur, ni les fumées ne doivent pouvoir se propager aux logements adjacents durant ce laps de temps.

Dans le cas d’une suppression du plafond plâtre, il faut donc vérifier que l’aire supérieure (la couche de plâtre située au-dessus des solives) est suffisamment étanche et résistante.

La création d'entrevous de finition entre solives peut être conçue pour compléter cet aspect.

### Stabilité mécanique

#### Poutres apparentes et mouvement de « pianotage » des solives

{{< figure src="/images/comprendre/poutrespianotage.webp">}}

Les planchers à structure bois ont été conçus à l’origine comme un élément complet dont les trois constituants principaux assurent ensemble la résistance et la rigidité :

- Les poutres et solives bois
- L’aire supérieure en plâtre, coulée sur lattis de châtaigner
- Le plafond plâtre sur bacula constituant la seconde membrane
    
On pourrait comparer cela aux deux faces en papier qui font la rigidité d’un carton ondulé.

La suppression du plafond risque donc de rendre le plancher plus souple, voire de faire vriller les solives.

Pour s'en prémunir, des entretoises et éléments structurels complémentaires peuvent être ajoutés.

### L’isolation phonique

#### Poutres apparentes et bruits de voisinage

L’isolation phonique par rapport aux voisins est le premier critère de confort d’un appartement.

Sur les vieux planchers, cette isolation est obtenue par l’addition de nos deux parois continues (plafond et aire en plâtre), éventuellement complétées par une chape supérieure formant un troisième barrage.

C’est la masse de ces éléments qui résiste aux vibrations et freine leur transmission. Ainsi, plus le plafond est épais, plus il est lourd et plus il isole. Il en va de même pour l’aire en plâtre ou la chape supérieure.

Traditionnellement, ces plâtres en plafond ont une épaisseur comprise entre 3 cm et 6 cm.

> Notons qu’un faux-plafond en plaque de plâtre est parfois mis en remplacement. Mais avec 13 mm d’épaisseur, il est 3 ou 4 fois moins lourd et n'apporte donc pas du tout le même confort.

Supprimer le plafond plâtre risque donc de détériorer l'isolation phonique et nécessite d'envisager un élément complémentaire.

{{< figure src="/images/comprendre/Bordy - Poutres apparentes.webp" title="Mettre les poutres apparentes nécessite quelques précautions">}}

## Exemples d'intervention

<ul class="pa0 mv0 w-100 justify-center flex flex-wrap">
<div class="relative w-40-ns w-100 mv2 mh4 mb4 bg-white nested-copy-line-height br3 shadow-1 grow card">
  <a href="https://debord.fr/realisations/greneta/" class="link black">
    <div class="bg-white mb3 overflow-hidden h-100 br3 cardtext">
      <h1 class="relatedtitle f3 tc normal">
        Confortement d'un plancher à poutres apparentes par le dessous
      </h1>
      <div class="nested-links f5 ph3 mt3 lh-copy nested-copy-line-height alegreya black tj">
        Renforcement de plancher en conservant l'apparence authentique.
      </div>
    </div>
  </a>
</div>
<div class="relative w-40-ns w-100 mv2 mh4 mb4 bg-white nested-copy-line-height br3 shadow-1 grow card">
  <a href="https://debord.fr/realisations/st-sebastien/" class="link black">
    <div class="bg-white mb3 overflow-hidden h-100 br3 cardtext">
      <h1 class="relatedtitle f3 tc normal">
        Confortement d'un plancher bois par le dessus
      </h1>
      <div class="nested-links f5 ph3 mt3 lh-copy nested-copy-line-height black alegreya tj">
        Renforcement d'un plancher par le dessus pour ne pas dégrader les poutres apparentes.
      </div>
    </div>
  </a>
</div>
</ul>
    
